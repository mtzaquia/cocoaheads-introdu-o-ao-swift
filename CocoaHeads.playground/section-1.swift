// Playground - noun: a place where people can play

import UIKit



// var and let
var variable = "Hello, playground"
variable = "Goodbye, objc!"

let constant = "Can't be changed"

var implicitDouble = 27.05
let explicitFloat: Float = 27.05
let boolValue = true


// arrays
var array = [String]()
array.append("✌️")
array.append("🍺")
print(array.count)

let staticArray = [String](count: 3, repeatedValue: "Frozen")



// dictionaries
var dictionary = [String:AnyObject]()
dictionary["newKey"] = "Value for that key"
dictionary["otherKey"] = 30
//dictionary["newKey"] = nil
println(dictionary)



// optional
var maybe: String?
println(maybe)

//maybe = "Now I'm here!"
println(maybe)
//println(maybe!)

if let alreadyUnwrapped = maybe {
	println(alreadyUnwrapped)
}



// tuples
var tuple = ("One", 2)
println(tuple.1)
var tupleWithNames = (first: "One", second: 2)
println(tupleWithNames.first)



// classes
class Person : NSObject , Singable {

	var name: String?
	var age: Int?
	var gender: Character?

	init(name: String, age: Int, gender: Character) {
		self.name = name
		self.age = age
		self.gender = gender
		super.init()
	//	self.sing()
	}

	convenience override init() {
		self.init(name: "Maurício Zaquia", age: 22, gender: "M")
	}


	// MARK: Singable
	func sing() {
		let lyrics = ["a, b, c!", "as easy as", "1, 2, 3!"]
		for i in 0...2 {
			println(lyrics[i])
		}
	}
	
}


// protocols
protocol Singable {
	func sing()
}


















